<?php
/**
 * @file
 * cit_content_types.features.inc
 */

/**
 * Implementation of hook_ctools_plugin_api().
 */
function cit_content_types_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => 1);
  }
}

/**
 * Implementation of hook_node_info().
 */
function cit_content_types_node_info() {
  $items = array(
    'publication' => array(
      'name' => t('Publication'),
      'base' => 'node_content',
      'description' => t('Publication meta-data'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'topic' => array(
      'name' => t('Topic'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
